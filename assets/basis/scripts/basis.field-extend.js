;(function($, window, document, undefined) {
	'use strict';

	basis.fieldExtend = {
		settings: {
			autoinit: true,
			main: '[data-field-extend]'
		},

		init: function() {
			this.handler();

			if ($(this.settings.main).length) {
				this.builder();
			}
		},

		handler: function() {
			var	that = this;

			$(document).on({
				change: function() {
					that.update($(this));
				}
			}, that.settings.main.replace(']', '*="__index"]'));
		},

		builder: function() {
			$(this.settings.main.replace(']', '="__index"]')).trigger('change');
		},

		update: function($this) {
			var	that = this,
				$form = $this.closest('form'),
				$fieldsets = $form.find('fieldset'),
				$fields = $form.find(that.settings.main.replace(']', '*="' + $this.attr('name') + '|"]'));

			$fields.each(function() {
				var	$label = $(this).closest('label'),
					field_value = $(this).data('fieldExtend').replace(/.+\|(.+)$/, '$1');

				if ($this.is(':disabled')) {
					$(this).attr('disabled', 'disabled').add($label).hide();
				} else {				
					if (field_value === $this.val()) {
						$(this).removeAttr('disabled').add($label).show();
					} else {
						$(this).attr('disabled', 'disabled').add($label).hide();
					}
				}

				if ($(this).data('fieldExtend').indexOf('__index') !== -1) {
					that.update($(this));
				}
			});

			$fieldsets.each(function() {
				var	fields_index = $(this).find(that.settings.main.replace(']', '="__index"]')).length,
					fields_extend = $(this).find(that.settings.main + ':not(' + that.settings.main.replace(']', '="__index"]') + '):not([disabled])').length,
					fields_normal = $(this).find(':input:not(' + that.settings.main + ')').length;

				if (fields_index || fields_extend || fields_normal) {
					$(this).show();
				} else {
					$(this).hide();
				}
			});
		}
	};
}(jQuery, this, this.document));

