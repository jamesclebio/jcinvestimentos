;(function($, window, document, undefined) {
	'use strict';

	basis.fieldMask = {
		settings: {
			autoinit: true,
			main: '[data-field-mask]'
		},

		init: function() {
			if ($(this.settings.main).length) {
				this.builder();
			}
		},

		builder: function() {
			$(this.settings.main).each(function() {
				switch ($(this).data('fieldMask')) {
					case 'date':
						$(this).mask('99/99/9999');
						break;

					case 'time':
						$(this).mask('99:99');
						break;

					case 'time-long':
						$(this).mask('99:99:99');
						break;

					case 'datetime':
						$(this).mask('99/99/9999 99:99');
						break;

					case 'datetime-long':
						$(this).mask('99/99/9999 99:99:99');
						break;

					case 'phone':
						$(this).mask('(99) 99999999?9');
						break;

					case 'cep':
						$(this).mask('99999-999');
						break;

					case 'cpf':
						$(this).mask('999.999.999-99');
						break;

					case 'cnpj':
						$(this).mask('99.999.999/9999-99');
						break;

					case 'currency-real':
						$(this).maskMoney({
							thousands: '.',
							decimal:',',
							allowZero: true
						});

						break;

					case 'currency-dollar':
						$(this).maskMoney({
							thousands: '',
							decimal:'.',
							allowZero: true
						});

						break;

					case 'weight':
						$(this).maskMoney({
							thousands: '',
							decimal:'.',
							allowZero: true,
							precision: 3
						});

						break;

					default:
						break;
				}
			});
		}
	};
}(jQuery, this, this.document));

