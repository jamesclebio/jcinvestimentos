;(function($, window, document, undefined) {
	'use strict';

	basis.dropdown = {
		settings: {
			autoinit: true,
			main: '.dropdown',
			class_active: 'is-active'
		},

		init: function() {
			this.handler();
		},

		handler: function() {
			var	that = this,
				$main = $(that.settings.main);

			// Dropdown
			$main.on({
				mouseenter: function() {
					$(this).clearQueue();
				},

				mouseleave: function() {
					$main.prev().removeClass(that.settings.class_active);
					$(this).fadeOut(0);
				}
			});

			// Trigger
			$main.prev().on({
				click: function(e) {
					if ($(this).attr('href') === '#') {
						e.preventDefault();
					}
				},

				mouseenter: function() {
					var $dropdown = $(this).next();

					if ($dropdown.length && !$dropdown.is(':visible')) {
						$(this).addClass(that.settings.class_active);
						$dropdown.clearQueue().fadeIn(0);
					}
				},

				mouseleave: function() {
					var	$this = $(this),
						$dropdown = $(this).next();

					if ($dropdown.is(':visible')) {
						$dropdown.delay(10).fadeOut(0, function() {
							$this.removeClass(that.settings.class_active);
						});
					}
				}
			});
		}
	};
}(jQuery, this, this.document));

