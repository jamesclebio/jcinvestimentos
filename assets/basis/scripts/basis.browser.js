;(function($, window, document, undefined) {
	'use strict';

	basis.browser = {
		settings: {
			autoinit: true,
			main: '[data-browser]'
		},

		init: function() {
			if ($(this.settings.main).length) {
				this.handler();
			}
		},

		handler: function() {
			var	that = this;

			$(this.settings.main).on({
				click: function(e) {
					switch ($(this).data('browser')) {
						case 'back':
							history.back();
							break;

						case 'forward':
							history.forward();
							break;

						case 'print':
							window.print();
							break;

						default:
							break;
					}

					e.preventDefault();
				}
			});
		}
	};
}(jQuery, this, this.document));

