;(function($, window, document, undefined) {
	'use strict';

	basis.rollHash = {
		settings: {
			autoinit: true,
			main: 'a[href*="#!"]',
			pattern_hashbang: /#!\/?(\w+).*/
		},

		init: function() {
			if ($(this.settings.main).length) {
				this.handler();
				this.builder();
			}
		},

		handler: function() {
			var	that = this;

			$(document).on({
				click: function() {
					basis.roll.to($(this).attr('href').replace(that.settings.pattern_hashbang, '#$1'));
				}
			}, that.settings.main);
		},

		builder: function() {
			var	target = (this.settings.pattern_hashbang.test(window.location.hash)) ? window.location.hash.replace(this.settings.pattern_hashbang, '#$1') : false;

			if (target) {
				basis.roll.to(target);
			}
		}
	};
}(jQuery, this, this.document));

