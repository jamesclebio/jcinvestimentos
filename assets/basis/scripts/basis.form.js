;(function($, window, document, undefined) {
	'use strict';

	basis.form = {
		settings: {
			autoinit: true,
			main: 'form',
			submit_prevent: 0
		},

		init: function() {
			this.handler();
		},

		handler: function() {
			var	that = this;

			// Submit prevent
			$(document).on({
				submit: function(e) {
					that.settings.submit_prevent++;

					if (that.settings.submit_prevent > 1) {
						e.preventDefault();
					}
				}
			}, that.settings.main);
		}
	};
}(jQuery, this, this.document));

