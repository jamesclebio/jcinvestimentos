;(function($, window, document, undefined) {
	'use strict';

	basis.fieldCheck = {
		settings: {
			autoinit: true,
			main: '#action-toggle',
			field: '.action-select',
			status_checked: '.field-check-checked',
			status_total: '.field-check-total',
			action_all: '.field-check-all',
			action_none: '.field-check-none',
			class_checked: 'is-checked'
		},

		init: function() {
			this.handler();

			if ($(this.settings.field).length) {
				this.builder();
			}
		},

		handler: function() {
			var	that = this;

			// Toggle
			$(document).on({
				change: function() {
					that.toggle();
				}
			}, that.settings.main);

			// Fields
			$(document).on({
				change: function() {
					that.check($(this));
				}
			}, that.settings.field);

			// Action all
			$(document).on({
				click: function(e) {
					$(that.settings.field).prop('checked', true).trigger('change');
					e.preventDefault();
				}
			}, that.settings.action_all);

			// Action none
			$(document).on({
				click: function(e) {
					$(that.settings.field).prop('checked', false).trigger('change');
					e.preventDefault();
				}
			}, that.settings.action_none);
		},

		builder: function() {
			var	that = this,
				$fields = $(this.settings.field);

			$fields.each(function() {
				if ($(this).prop('checked')) {
					$(this).closest('tr').addClass(that.settings.class_checked);
				}
			});

			$(that.settings.status_total).text($fields.length);

			that.status();
		},

		toggle: function() {
			var	that = this,
				$main = $(this.settings.main),
				$fields = $(that.settings.field);

			if ($main.prop('checked')) {
				$fields.prop('checked', true).closest('tr').addClass(that.settings.class_checked);
			} else {
				$fields.prop('checked', false).closest('tr').removeClass(that.settings.class_checked);
			}

			that.status();
		},

		check: function($this) {
			var	that = this,
				$main = $(this.settings.main),
				$fields = $(that.settings.field);

			if ($this.prop('checked')) {
				$this.closest('tr').addClass(that.settings.class_checked);
			} else {
				$this.closest('tr').removeClass(that.settings.class_checked);
			}

			if ($(that.settings.field + ':not(:checked)').length) {
				$main.prop('checked', false);
			} else {
				$main.prop('checked', true);
			}

			that.status();
		},

		status: function() {
			$(this.settings.status_checked).text($(this.settings.field + ':checked').length);
		}
	};
}(jQuery, this, this.document));

