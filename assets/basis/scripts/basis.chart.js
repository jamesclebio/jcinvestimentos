;(function($, window, document, undefined) {
    'use strict';

    basis.chart = {
        settings: {
            autoinit: true,
            main: '[data-chart]',
            toggle: '[data-chart-toggle]',
            template_loading: '<div class="alert-short alert-short-loading"><p>Carregando gráfico...</p></div>',
            template_error: '<div class="alert-short alert-short-error"><h3>Ops!</h3><p>Houve um erro ao carregar gráfico... :(</p></div>'
        },

        init: function() {
            if ($(this.settings.main).length) {
                this.handler();
                this.builder();
            }
        },

        handler: function() {
            var that = this;

            // Toggle
            $(this.settings.toggle).on({
                change: function() {
                    that.toggle($(this).closest('div').find(that.settings.main), $(this).val());
                }
            });
        },

        builder: function() {
            var that = this,
                $main = $(this.settings.main);

            $main.each(function() {
                that.draw($(this));
            });
        },

        draw: function($this) {
            var that = this,
                url = $this.data('chart');

            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'json',

                beforeSend: function() {
                    $this.html(that.settings.template_loading);
                },

                error: function() {
                    $this.html(that.settings.template_error);
                },

                success: function(data) {
                    $this.highcharts(data);
                }
            });
        },

        toggle: function($target, chart) {
            $target.data('chart', chart);
            this.draw($target);
        }
    };
}(jQuery, this, this.document));

