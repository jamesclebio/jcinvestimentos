var	main = main || {};

;(function($, window, document, undefined) {
	'use strict';

	main.selectName = {
		settings: {
			autoinit: true,
			main: 'select[data-name]'
		},

		init: function() {
			if ($(this.settings.main).length) {
				this.handler();
				this.builder();
			}
		},

		handler: function() {
			var	that = this,
				$main = $(this.settings.main);

			$main.on({
				change: function() {
					that.name($(this));
				}
			});
		},

		builder: function() {
			var	that = this,
				$main = $(this.settings.main);

			$main.each(function() {
				that.name($(this));
			});
		},

		name: function($this) {
			if ($this.val()) {
				$this.attr('name', $this.find('option[value="' + $this.val() + '"]').data('name'));
			} else {
				$this.removeAttr('name');
			}
		}
	};
}(jQuery, this, this.document));

