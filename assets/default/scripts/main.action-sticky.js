var	main = main || {};

;(function($, window, document, undefined) {
	'use strict';

	main.actionSticky = {
		settings: {
			autoinit: true,
			main: '.action-sticky',
			main_top: null,
			class_sticky: 'is-sticky'
		},

		init: function() {
			if ($(this.settings.main).length) {
				this.handler();
				this.builder();
			}
		},

		handler: function() {
			var	that = this,
				$main = $(this.settings.main);

			$(window).on({
				scroll: function() {
					that.sticky($main);
				}
			});
		},

		builder: function() {
			this.settings.main_top = $(this.settings.main).position().top;
		},

		sticky: function($main) {
			var	that = this;

			if ($(window).scrollTop() >= this.settings.main_top) {
				$main.addClass(that.settings.class_sticky);
				$('body').css('padding-top', $main.outerHeight(true));
			} else {
				$main.removeClass(that.settings.class_sticky);
				$('body').css('padding-top', 0);
				this.settings.main_top = $(this.settings.main).position().top;
			}
		}
	};
}(jQuery, this, this.document));

