var	main = main || {};

;(function($, window, document, undefined) {
	'use strict';

	main.nav = {
		settings: {
			autoinit: true,
			main: '.main-nav',
			class_sub: 'is-sub',
			class_open: 'is-open',
			class_current: 'is-current'
		},

		init: function() {
			if ($(this.settings.main).length) {
				this.handler();
				this.builder();
			}
		},

		handler: function() {
			var	that = this,
				$main = $(this.settings.main);

			$main.find('li:has(ul) > a[href="#"]').on({
				click: function(e) {
					that.toggle($(this));
					e.preventDefault();
				}
			});
		},

		builder: function() {
			var $main = $(this.settings.main);

			$main.find('li:has(ul)').addClass(this.settings.class_sub);
			$main.find('li:has(.' + this.settings.class_current + ')').addClass(this.settings.class_open);
		},

		toggle: function($this) {
			var	that = this,
				$main = $(this.settings.main),
				$item = $this.parent(),
				$sub = $item.find('ul');

			if ($item.hasClass(that.settings.class_open)) {
				$sub.slideUp(100, function() {
					$item.removeClass(that.settings.class_open);
				});
			} else {
				$item.parent().find('ul:visible').slideUp(100, function() {
					$(this).parent().removeClass(that.settings.class_open);
				});

				$sub.slideDown(100, function() {
					$item.addClass(that.settings.class_open);
				});
			}
		}
	};
}(jQuery, this, this.document));

