var main = main || {};

;(function($, window, document, undefined) {
    'use strict';

    main.financeTransactions = {
        settings: {
            autoinit: true,
            main: '[data-finance-transactions]'
        },

        init: function() {
            if ($(this.settings.main).length) {
                this.handler();
                this.builder();
            }
        },

        handler: function() {
            var that = this,
                $main = $(this.settings.main);

            $(that.mainSelector('addon')).on({
                click: function(e) {
                    that.addon();
                    e.preventDefault();
                }
            });
        },

        builder: function() {
            var that = this,
                $main = $(this.settings.main);


        },

        mainSelector: function(dataValue) {
            return this.settings.main.replace(']', '="' + dataValue + '"]');
        },

        newIndex: function() {
            var $currentIndex = $(this.mainSelector('transaction') + ':last [name*="transaction"]:first').attr('name');

            if ($currentIndex) {
                return $currentIndex.replace('/transaction(\d+)_.+/', '$1') + 1;
            } else {
                return 1;
            }
        },

        addon: function() {
            var $template = $(this.mainSelector('template')),
                $index = this.newIndex(),
                $transaction = $template.clone().appendTo($template.closest('form'));

            console.log($index);

            $transaction
                .attr(this.settings.main.replace(/\[(.+)\]/, '$1'), 'transaction')
                .find(this.mainSelector('installments')).hide();
        }
    };
}(jQuery, this, this.document));

