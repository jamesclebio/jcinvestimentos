<!doctype html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?> no-js" lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $this->getTitle(); ?></title>
    <meta name="description" content="<?php echo $this->getDescription(); ?>">
    <meta name="robots" content="noindex">
    <link rel="stylesheet" href="<?php echo $this->_asset('default/styles/main.css'); ?>">
    <script src="<?php echo $this->_asset('default/scripts/head.js'); ?>"></script>
    <?php $this->getHeadAppend(); ?>
</head>
<body<?php $this->getBodyAttribute(); ?> class="<?php $this->getBodyClass(); ?>">
    <?php $this->getAnalytics(); ?>
    <?php $this->getBodyPrepend(); ?>

    <header class="default-header">
        <ul class="nav">
            <li><a href="#"><span class="icon-android-notifications"></span><span class="tag tag-warning">3</span></a>
                <div class="dropdown dropdown-large">
                    <!-- <div class="alert-short alert-short-empty">
                        <p>Nenhuma nova notificação.</p>
                    </div> -->

                    <ul class="list-notifier">
                        <li>
                            <h5>Lorem ipsum dolor</h5>
                            <ul>
                                <li>
                                    <a href="#"><strong>Lorem ipsum dolor</strong></a>
                                    <ul class="list-link-inline">
                                        <li><a href="#">Lorem</a></li>
                                        <li><a href="#">Ipsum</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#"><strong>Lorem ipsum dolor</strong></a>
                                    <ul class="list-link-inline">
                                        <li><a href="#">Lorem</a></li>
                                        <li><a href="#">Ipsum</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <h5>Lorem ipsum dolor</h5>
                            <ul>
                                <li>
                                    <a href="#"><strong>Lorem ipsum dolor</strong></a>
                                    <ul class="list-link-inline">
                                        <li><a href="#">Lorem</a></li>
                                        <li><a href="#">Ipsum</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#"><strong>Lorem ipsum dolor</strong></a>
                                    <ul class="list-link-inline">
                                        <li><a href="#">Lorem</a></li>
                                        <li><a href="#">Ipsum</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </li>
            <li><a href="#"><span class="icon-ios7-person"></span> James Clébio</a>
                <div class="dropdown">
                    <ul class="list-short">
                        <li><a href="#">Editar conta</a></li>
                        <li><a href="#">Alterar senha</a></li>
                        <li><a href="#"><strong>Sair</strong></a></li>
                    </ul>
                </div>
            </li>
            <li><a href="#"><span class="icon-help-circled"></span> Ajuda</a>
                <div class="dropdown">
                    <ul class="list-short">
                        <li><a href="#">Dúvidas frequentes</a></li>
                        <li><a href="#">Suporte</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </header>

    <aside class="default-aside">
        <h1 class="main-logo"><a href="<?php echo $this->_url('root'); ?>">JC Investimentos</a></h1>

        <nav class="main-nav">
            <ul>
                <li class="is-current"><a href="<?php echo $this->_url('root'); ?>"><span class="icon-stats-bars"></span> Painel</a></li>
                <li><a href="#"><span class="icon-social-usd"></span> Financeiro</a>
                    <ul>
                        <li><a href="#">Casas</a></li>
                        <li><a href="#">Vendedores</a></li>
                        <li><a href="#">Clientes</a></li>
                        <li><a href="#">Usuários</a></li>
                    </ul>
                </li>
                <li><a href="#"><span class="icon-clipboard"></span> Estoque</a></li>
                <li><a href="#"><span class="icon-document"></span> Cadastro</a>
                    <ul>
                        <li><a href="#">Casas</a></li>
                        <li><a href="#">Vendedores</a></li>
                        <li><a href="#">Clientes</a></li>
                        <li><a href="#">Usuários</a></li>
                    </ul>
                </li>
                <li><a href="#"><span class="icon-pin"></span> Sample</a>
                    <ul>
                        <li><a href="<?php echo $this->_url('sample/table'); ?>">Table</a></li>
                        <li><a href="<?php echo $this->_url('sample/form'); ?>">Form</a></li>
                        <li><a href="<?php echo $this->_url('sample/confirm'); ?>">Confirm</a></li>
                        <li><a href="<?php echo $this->_url('sample/help'); ?>">Help</a></li>
                        <li><a href="<?php echo $this->_url('sign/in'); ?>">Sign in</a></li>
                        <li><a href="<?php echo $this->_url('password/remember'); ?>">Password remember</a></li>
                        <li><a href="<?php echo $this->_url('password/reset'); ?>">Password reset</a></li>
                        <li><a href="<?php echo $this->_url('finance/transactions'); ?>">Finance transactions</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
    </aside>

    <div class="default-content">
        <div class="default-content-container">
            <div class="breadcrumb">
                <ul>
                    <li><a href="#">Home</a></li>
                    <li>Current page</li>
                </ul>
            </div>

            <div class="alert alert-success" data-alert-close="true">
                <p><strong>Lorem ipsure recusandae provident iusto asperiores dolor deserunt.</strong></p>
            </div>

            <?php $this->getView(); ?>
        </div>
    </div>

    <button type="button" data-toggle-roll="40|display-block" data-roll-top title="Ir para o topo"></button>

    <script src="<?php echo $this->_asset('default/scripts/main.js'); ?>"></script>
    <?php $this->getBodyAppend(); ?>
</body>
</html>