<!doctype html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?> layout-lite no-js" lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $this->getTitle(); ?></title>
	<meta name="description" content="<?php echo $this->getDescription(); ?>">
	<link rel="stylesheet" href="<?php echo $this->_asset('default/styles/main.css'); ?>">
	<script src="<?php echo $this->_asset('default/scripts/head.js'); ?>"></script>
	<?php $this->getHeadAppend(); ?>
</head>
<body<?php $this->getBodyAttribute(); ?> class="<?php $this->getBodyClass(); ?>">
	<?php $this->getAnalytics(); ?>
	<?php $this->getBodyPrepend(); ?>

	<header class="lite-header">
		<h1 class="main-logo"><a href="<?php echo $this->_url('root'); ?>">JC Investimentos</a></h1>
	</header>

	<div class="lite-content">
		<?php $this->getView(); ?>
	</div>

	<script src="<?php echo $this->_asset('default/scripts/main.js'); ?>"></script>
	<?php $this->getBodyAppend(); ?>
</body>
</html>