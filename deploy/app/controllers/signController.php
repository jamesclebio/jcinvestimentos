<?php
class Sign extends Page
{
	public function index() {
		$this->setLayout('lite');
		$this->setView(null);
		$this->setTitle('JC Investimentos');
		$this->setDescription('');
		$this->setAnalytics(null);
	}

	public function in() {
		$this->setView('sign-in');
	}

	public function out() {}

	public function up() {
		$this->setView('sign-up');
	}
}

