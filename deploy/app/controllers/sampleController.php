<?php
class Sample extends Page
{
	public function index() {
		$this->setLayout('default');
		$this->setView('dashboard');
		$this->setTitle('JC Investimentos');
		$this->setDescription('');
		$this->setAnalytics(null);
	}

	// Table
	public function table() {
		$this->setView('sample/table');
	}

	// Form
	public function form() {
		$this->setView('sample/form');
	}

	// Confirm
	public function confirm() {
		$this->setView('sample/confirm');
	}

	// Help
	public function help() {
		$this->setView('sample/help');
	}

	// Chart
	public function chart() {
		$this->setLayout(null);
		$this->setView(null);

		switch ($this->_get('type')) {
			case 'line':
				$this->setView('sample/chart-line');
				break;

			case 'column':
				$this->setView('sample/chart-column');
				break;

			case 'pie':
				$this->setView('sample/chart-pie');
				break;

			default:
				break;
		}
	}

	// Popup
	public function popup() {
		$this->setLayout('popup');
		$this->setView('sample/form');
	}
}

