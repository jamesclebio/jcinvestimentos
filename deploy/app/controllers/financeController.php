<?php
class Finance extends Page
{
	public function index() {
		$this->setLayout(null);
		$this->setView(null);
		$this->setTitle('JC Investimentos');
		$this->setDescription('');
		$this->setAnalytics(null);
	}

	public function transactions() {
		$this->setLayout('default');
		$this->setView('finance-transactions');
	}
}

