<?php
class Password extends Page
{
	public function index() {
		$this->setLayout('lite');
		$this->setView(null);
		$this->setTitle('JC Investimentos');
		$this->setDescription('');
		$this->setAnalytics(null);
	}

	public function reset() {
		$this->setView('password-reset');
	}

	public function remember() {
		$this->setView('password-remember');
	}
}

