<?php
/**
 * Basis
 *
 * @author James Clébio <jamesclebio@gmail.com>
 * @link https://github.com/jamesclebio/basis 
 * @license http://jamesclebio.mit-license.org/
 */

class Singleton
{
	public static function getInstance() {
		static $instance = null;

		if (null === $instance) {
			$instance = new static();
		}

		return $instance;
	}

	private function __construct() {}

	private function __clone() {}

	private function __wakeup() {}
}

