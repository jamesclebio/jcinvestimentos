<?php
/**
 * Basis
 *
 * @author James Clébio <jamesclebio@gmail.com>
 * @link https://github.com/jamesclebio/basis 
 * @license http://jamesclebio.mit-license.org/
 */

class Init
{
	private $root;
	private $get;
	private $controller;
	private $action;

	public function __construct($controller_routes = array()) {
		$this->setTimezone();
		$this->setRoot();
		$this->explodeGet();
		$this->setController();
		$this->setAction();
		$this->hookGet();
		$this->getController($controller_routes);
		$this->page();
	}

	private function setTimezone() {
		date_default_timezone_set(TIMEZONE);
	}

	private function setRoot() {
		$path = pathinfo($_SERVER['SCRIPT_NAME']);

		$this->root = ($path['dirname'] == '/') ? $path['dirname'] : $path['dirname'] . '/';
	}

	private function explodeGet() {
		$get = (isset($_GET['get'])) ? $_GET['get'] : 'index';

		$this->get = explode('/', $get);
	}

	private function hookGet() {
		$index_list = array();
		$value_list = array();
		$i = 0;

		unset($this->get[0], $this->get[1]);

		if (!empty($this->get)) {
			if (end($this->get) == null)
				array_pop($this->get);

			foreach ($this->get as $value) {
				if ($i % 2 == 0)
					array_push($index_list, $value);
				else 
					array_push($value_list, $value);

				$i++;
			}
		}

		if (!empty($index_list)) {
			if (count($index_list) > count($value_list))
				array_push($value_list, null);

			$this->get = array_combine($index_list, $value_list);
		} else
			$this->get = array();
	}

	private function getController($controller_routes) {
		$controller = CONTROLLERS . str_replace('-', '', $this->controller) . 'Controller.php';

		if (!file_exists($controller)) {
			if (array_key_exists($this->controller, $controller_routes)) {
				$this->controller = str_replace('-', '', $controller_routes[$this->controller]);
				$controller = CONTROLLERS . $this->controller . 'Controller.php';
			} else {
				header('HTTP/1.0 404 Not Found');
				readfile('404.html');
				exit;
			}
		} else
			$this->controller = str_replace('-', '', $this->controller);

		require_once $controller;
	}

	private function setController() {
		$this->controller = $this->get[0];
	}

	private function getAction($page) {
		$action = $this->action;

		if ($action) {
			if (!method_exists($page, $action))
				exit('URL error: no action');

			$page->$action();
		}
	}

	private function setAction() {
		$this->action = (!isset($this->get[1]) || $this->get[1] == null || $this->get[1] == 'index') ? false : str_replace('-', '', $this->get[1]);
	}

	private function page() {
		$page = new $this->controller();

		$page->setRoot($this->root);
		$page->setGet($this->get);
		$page->setNameController($this->controller);
		$page->setNameAction($this->action);
		$page->index();
		$page->getSession();
		$page->getAuthentication();
		$this->getAction($page);
		$page->getLayout();
		$page->getView();
	}
}

