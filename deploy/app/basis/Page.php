<?php
/**
 * Basis
 *
 * @author James Clébio <jamesclebio@gmail.com>
 * @link https://github.com/jamesclebio/basis 
 * @license http://jamesclebio.mit-license.org/
 */

class Page
{
	private $name_controller;
	private $name_action;
	private $root;
	private $get;
	private $session = false;
	private $authentication = false;
	private $layout = 'default';
	private $view = 'index';
	private $title;
	private $description;
	private $html_attribute;
	private $html_class;
	private $head_append;
	private $body_attribute;
	private $body_class;
	private $body_prepend;
	private $body_append;
	private $analytics = false;

	public function getNameController() {
		return $this->name_controller;
	}

	public function setNameController($name_controller) {
		$this->name_controller = $name_controller;
	}

	public function getNameAction() {
		return $this->name_action;
	}

	public function setNameAction($name_action) {
		$this->name_action = $name_action;
	}

	public function setRoot($root) {
		$this->root = $root;
	}

	public function setGet($get) {
		$this->get = $get;
	}

	public function getSession() {
		if ($this->session) {
			session_start();
		}
	}

	protected function setSession($session = false) {
		$this->session = $session;
	}

	public function getAuthentication() {
		if ($this->authentication) {
			AuthenticationHelper::sessionValidate();
		}
	}

	protected function setAuthentication($authentication = false) {
		$this->authentication = $authentication;
	}

	public function getLayout() {
		if ($this->layout) {
			require_once LAYOUTS . $this->layout . '.php';
		}
	}

	protected function setLayout($layout) {
		$this->layout = $layout;
	}

	public function getView() {
		if ($this->view) {
			require_once VIEWS . $this->view . '.php';
		}
	}

	protected function setView($view) {
		$this->view = $view;
	}

	protected function getTitle() {
		return $this->title;
	}

	protected function setTitle($title) {
		$this->title = $title;
	}

	protected function getDescription() {
		return $this->description;
	}

	protected function setDescription($description) {
		$this->description = $description;
	}

	protected function getHtmlAttribute() {
		echo ' ', $this->html_attribute;
	}

	protected function setHtmlAttribute($html_attribute) {
		$this->html_attribute = $html_attribute;
	}

	protected function getHtmlClass() {
		echo $this->html_class;
	}

	protected function setHtmlClass($html_class) {
		$this->html_class = $html_class;
	}

	protected function getBodyAttribute() {
		echo ' ', $this->body_attribute;
	}

	protected function setBodyAttribute($body_attribute) {
		$this->body_attribute = $body_attribute;
	}

	protected function getBodyClass() {
		echo $this->body_class;
	}

	protected function setBodyClass($body_class) {
		$this->body_class = $body_class;
	}

	protected function getHeadAppend() {
		echo $this->head_append;
	}

	protected function setHeadAppend($head_append) {
		$this->head_append = $head_append;
	}

	protected function getBodyPrepend() {
		echo $this->body_prepend;
	}

	protected function setBodyPrepend($body_prepend) {
		$this->body_prepend = $body_prepend;
	}

	protected function getBodyAppend() {
		echo $this->body_append;
	}

	protected function setBodyAppend($body_append) {
		$this->body_append = $body_append;
	}

	protected function getAlert() {
		if (!empty($_SESSION['alert'])) {
			echo '<div class="alert alert-' . $_SESSION['alert']['type'] . '"><a href="#" class="close" title="Fechar alerta">x</a>' . $_SESSION['alert']['content'] . '</div>';
			$_SESSION['alert'] = '';
		}
	}

	public function setAlert($content = null, $type = 'warning') {
		$_SESSION['james'] = 'jamesssss';
		$_SESSION['alert'] = array('content' => $content, 'type' => $type);
	}

	protected function getAnalytics() {
		if ($this->analytics) {
			include 'analytics.php';
		}
	}

	protected function setAnalytics($analytics = false) {
		$this->analytics = $analytics;
	}

	public function _get($index = false) {
		if ($index) {
			return (array_key_exists($index, $this->get)) ? $this->get[$index] : null;
		} else {
			return $this->get;
		}
	}

	public function _url($target = null) {
		return ($target != 'root') ? $this->root . $target : $this->root;
	}

	public function _asset($target = null) {
		return $this->root . ASSETS . $target;
	}

	public function _include($target = null) {
		return $this->root . INCLUDES . $target;
	}

	protected function cacheInstance() {
		file_put_contents(CACHE . 'instance-page-' . $this->name, serialize($this));
	}
}

