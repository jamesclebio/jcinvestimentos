<div class="heading-page">
	<h2>Entrar</h2>
</div>

<!-- <div class="alert alert-danger" data-alert-close="true">
	<p><strong>Dados de acesso inválidos!</strong></p>
	<p>Verifique as informações e tente novamente.</p>
</div> -->

<div class="block-box">
	<form id="" method="post" action="" class="form">
		<fieldset>
			<legend>Entrar</legend>

			<label>
				<div class="field-icon field-icon-large">
					<span class="icon-person"></span>
					<input name="user" type="text" placeholder="Usuário (Email)" class="field-large" required>
				</div>
			</label>

			<label>
				<div class="field-icon field-icon-large">
					<span class="icon-locked"></span>
					<input name="password" type="password" placeholder="Senha" class="field-large" required>
				</div>
			</label>

			<div class="block-action">
				<ul class="list-link-inline">
					<li><a href="<?php echo $this->_url('password/remember'); ?>">Esqueceu a senha?</a></li>
				</ul>
				<button type="submit" class="button button-large button-info">Entrar</button>
			</div>
		</fieldset>
	</form>
</div>

