<div class="heading-page">
	<h2>Financeiro - Transações</h2>
</div>

<form action="" class="form">
	<div class="action-sticky">
		<div class="action-sticky-container">
			<button type="submit" name="_save" class="button button-success">Save</button>
			<button type="submit" name="_continue" class="button">Save and continue editing</button>
			<button type="submit" name="_addanother" class="button">Save and add another</button>
			<a href="delete/" class="button button-danger" title="Delete"><span class="icon-trash-a"></span></a>
		</div>
	</div>

	<div class="box-group align-right">
		<button type="button" class="button button-info" data-finance-transactions="addon"><span class="icon-plus-circled"></span> Nova transação</button>
	</div>

	<!-- <div class="alert-short alert-short-empty">
        <h3>Ops!</h3>
        <p>Nenhuma transação financeira para exibir aqui.</p>
    </div> -->

	<!-- Transaction -->
	<div class="separate" data-finance-transactions="template">
		<div class="block-box">
			<div class="grid grid-items-8">
				<div class="grid-item grid-item-span-2">
					<label>Valor
						<input name="transaction0_amount" type="text" data-field-mask="currency-real" value="0,00">
						<div class="margin-top-5 text-small">
							<label class="check inline"><input name="transaction0_amountType" type="radio" checked>Total</label>
							<label class="check inline"><input name="transaction0_amountType" type="radio">Parcela</label>
						</div>
					</label>
				</div>
				<div class="grid-item">
					<label>Parcelas<input name="transaction0_installments" type="number" value="1"></label>
				</div>
				<div class="grid-item">
					<label>Frequência
						<select name="">
							<option value="1" selected>Mensal</option>
							<option value="2">Trimestral</option>
							<option value="3">Semestral</option>
							<option value="4">Anual</option>
						</select>
					</label>
				</div>
				<div class="grid-item">
					<label>Data pagto
						<input name="transaction0_payDate" type="text" data-field-picker="date" data-field-mask="date">
						<span class="field-helper-info">1ª parcela</span>
					</label>
				</div>
				<div class="grid-item">
					<label class="check margin-top-25"><input name="transaction0_paid" type="checkbox">Pago</label>
				</div>

				<div class="float-right">
					<button type="button" class="button button-inline button-warning" data-finance-transactions="installments-generate" disabled>Gerar parcelas</button>
					<button type="button" class="button button-inline button-danger" data-finance-transactions="transaction-remove" disabled><span class="icon-trash-a"></span></button>
				</div>
			</div>

			<!-- Installments -->
			<div class="collapse is-open" data-finance-transactions="installments">
				<h4 class="collapse-heading">Parcelas 1-10</h4>
				<div class="collapse-container">
					<div class="grid grid-items-8" data-finance-transactions="installments-item">
						<div class="grid-item">
							<div class="margin-top-15 align-right"><strong>1.</strong></div>
						</div>
						<div class="grid-item grid-item-span-2">
							<label><input name="transaction0_installment1_amount" type="text" data-field-mask="currency-real" value="0,00" required title="Valor"></label>
						</div>
						<div class="grid-item">
							<label><input name="transaction0_installment1_payDate" type="text" data-field-picker="date" data-field-mask="date" value="10/10/1000" required title="Data pagto"></label>
						</div>
						<div class="grid-item">
							<label class="check margin-top-15"><input name="transaction0_installment1_paid" type="checkbox">Pago</label>
						</div>
					</div>
				</div>
			</div>
			<!-- /Installments -->

		</div>
	</div>
	<!-- /Transaction -->

</form>
