<div class="heading-page">
    <h2>Painel</h2>
</div>

<div class="block-group">
    <div class="grid grid-items-3">
        <div class="grid-item">
            <div class="block-box height-130">
                <div class="text">
                    <h4 class="color-info"><span class="icon-person"></span> Olá, James!</h4>
                    <p>Seu último acesso foi em <strong>22 de novembro de 2014</strong>, às <strong>22:22:22</strong>.</p>

                    <div class="separate align-right">
                        <div class="list-link-inline">
                            <li><a href="#" class="link"><span class="icon-edit"></span> Editar conta</a></li>
                            <li><a href="#" class="link"><span class="icon-locked"></span> Alterar senha</a></li>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid-item">
            <div class="block-box height-130">
                <h4 class="heading color-light"><span class="icon-star"></span> Casas</h4>

                <form action="" class="form">
                    <fieldset>
                        <legend>Pesquisa</legend>
                        <div class="field-merged">
                            <input name="q" type="text" placeholder="Pesquisa rápida">
                            <button type="submit" class="button button-info"><span class="icon-search"></span></button>
                        </div>
                    </fieldset>
                </form>

                <div class="separate align-right">
                    <div class="list-link-inline">
                        <li><a href="#" class="link"><span class="icon-navicon-round"></span> Listar</a></li>
                        <li><a href="#" class="link"><span class="icon-plus-circled"></span> Adicionar</a></li>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid-item">
            <div class="block-box height-130">
                <h4 class="heading color-light"><span class="icon-star"></span> Clientes</h4>

                <form action="" class="form">
                    <fieldset>
                        <legend>Pesquisa</legend>
                        <div class="field-merged">
                            <input name="q" type="text" placeholder="Pesquisa rápida">
                            <button type="submit" class="button button-info"><span class="icon-search"></span></button>
                        </div>
                    </fieldset>
                </form>

                <div class="separate align-right">
                    <div class="list-link-inline">
                        <li><a href="#" class="link"><span class="icon-navicon-round"></span> Listar</a></li>
                        <li><a href="#" class="link"><span class="icon-plus-circled"></span> Adicionar</a></li>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="block-group block-box">
    <div class="block-bordered">
        <h5 class="heading">Chart 1
            <select data-chart-toggle class="float-right">
                <option value="<?php echo $this->_url('sample/chart/type/line'); ?>">Chart 1</option>
                <option value="<?php echo $this->_url('sample/chart/type/line'); ?>">Chart 2</option>
                <option value="<?php echo $this->_url('sample/chart/type/line'); ?>">Chart 3</option>
            </select>
        </h5>

        <div class="separate">
            <div data-chart="<?php echo $this->_url('sample/chart/type/line'); ?>"></div>
        </div>
    </div>
</div>

<div class="block-group">
    <div class="grid grid-items-2">
        <div class="grid-item">
            <div class="block-box">
                <div class="block-bordered">
                    <h5 class="heading">Chart 2
                        <select data-chart-toggle class="float-right">
                            <option value="<?php echo $this->_url('sample/chart/type/pie'); ?>">Chart 1</option>
                            <option value="<?php echo $this->_url('sample/chart/type/pie'); ?>">Chart 2</option>
                            <option value="<?php echo $this->_url('sample/chart/type/pie'); ?>">Chart 3</option>
                        </select>
                    </h5>

                    <div class="separate">
                        <div data-chart="<?php echo $this->_url('sample/chart/type/pie'); ?>"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid-item">
            <div class="block-box">
                <div class="block-bordered">
                    <h5 class="heading">Chart 3
                        <select data-chart-toggle class="float-right">
                            <option value="<?php echo $this->_url('sample/chart/type/column'); ?>">Chart 1</option>
                            <option value="<?php echo $this->_url('sample/chart/type/column'); ?>">Chart 2</option>
                            <option value="<?php echo $this->_url('sample/chart/type/column'); ?>">Chart 3</option>
                        </select>
                    </h5>

                    <div class="separate">
                        <div data-chart="<?php echo $this->_url('sample/chart/type/column'); ?>"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
