<section class="section-content" id="como-funciona">
	<div class="section-content-container">
		<header>
			<h1>Lembrar senha</h1>
		</header>

		<!-- <div class="alert alert-error" data-alert-close="true">
			<p><strong>Alert message here!</strong></p>
			<p>Secondary alert message here.</p>
		</div> -->

		<form id="form-password" method="post" action="" class="form">
			<fieldset>
				<legend>Senha</legend>
				
				<div class="grid grid-items-2">
					<div class="grid-item">
						<label>Usuário (Email) *<input name="user" type="text" required></label>
					</div>
				</div>

				<div class="pane-action">
					<button type="submit" class="button">Lembrar</button>
				</div>
			</fieldset>
		</form>
	</div>
</section>
