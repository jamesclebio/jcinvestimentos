<div class="heading-page">
    <h2>Form</h2>
</div>

<form action="" class="form">
	<div class="action-sticky">
		<div class="action-sticky-container">
			<button type="submit" name="_save" class="button button-success">Save</button>
			<button type="submit" name="_continue" class="button">Save and continue editing</button>
			<button type="submit" name="_addanother" class="button">Save and add another</button>
			<a href="delete/" class="button button-danger" title="Delete"><span class="icon-trash-a"></span></a>
		</div>
	</div>

	<fieldset class="field-group">
		<legend>Picker</legend>

		<label>Date<input name="" type="text" data-field-picker="date"></label>
		<label>Datetime<input name="" type="text" data-field-picker="datetime"></label>
	</fieldset>

	<fieldset class="field-group">
		<legend>Mask</legend>

		<label>Date<input name="" type="text" data-field-mask="date"></label>
		<label>Time (hour:minute)<input name="" type="text" data-field-mask="time-pair-2"></label>
		<label>Time (hour:minute:second)<input name="" type="text" data-field-mask="time-pair-3"></label>
		<label>CEP<input name="" type="text" data-field-mask="cep"></label>
		<label>Phone<input name="" type="text" data-field-mask="phone"></label>
		<label>CNPJ<input name="" type="text" data-field-mask="cnpj"></label>
		<label>CPF<input name="" type="text" data-field-mask="cpf"></label>
		<label>Money<input name="" type="text" data-field-mask="money"></label>
		<label>Weight<input name="" type="text" data-field-mask="weight"></label>
	</fieldset>
</form>

