{
    "chart": {
        "type": "pie"
    },

    "credits": {
        "enabled": false
    },

    "title": {
        "text": null
    },

    "tooltip": {
        "pointFormat": "{point.percentage:.1f}%"
    },

    "plotOptions": {
        "pie": {
            "showInLegend": false,
            "allowPointSelect": false,
            "cursor": "pointer",
            "dataLabels": {
                "enabled": true,
                "color": "#000000",
                "connectorColor": "#000000",
                "format": "<b>{point.name}</b>: {point.percentage:.1f}%"
            }
        }
    },

    "series": [{
        "data": [
            ["Enviadas", 45.0],
            ["Rascunho", 33.7],
            {
                "name": "Em Envio",
                "y": 10.8,
                "sliced": true,
                "selected": true
            },
            ["Canceladas", 8.5],
            ["Pausadas", 2.8]
        ]
    }]
}

